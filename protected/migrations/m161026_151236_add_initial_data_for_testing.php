<?php

use app\models\Patient;
use app\models\Report;

class m161026_151236_add_initial_data_for_testing extends \yii\db\Migration {

	public function safeUp() {
		$this->execute('SET FOREIGN_KEY_CHECKS=0;');
		$this->truncateTable(Report::tableName());
		$this->truncateTable(Patient::tableName());
		$this->execute('SET FOREIGN_KEY_CHECKS=1;');

		$content = [
			[
				1,
				'michael',
				'michael@gmail.com',
				'58108d5b58cbd4.15908389',
			],
			[
				2,
				'jack',
				'jack@gmail.com',
				'68108d5b58cbd4.15908389',
			],
			[
				3,
				'tom',
				'tom@gmail.com',
				'78108d5b58cbd4.15908389',
			],
			[
				4,
				'john',
				'john@gmail.com',
				'88108d5b58cbd4.15908389',
			],
			[
				5,
				'andrew',
				'andrew@gmail.com',
				'98108d5b58cbd4.15908389',
			],
		];
		foreach ($content as $item) {
			$this->insert(Patient::tableName(), [
				'id'       => $item[0],
				'username' => $item[1],
				'email'    => $item[2],
				'authKey'  => $item[3],
			]);
		}

		$content = [
			[
				'Report about blood',
				'Blood is okay, dark red very good',
				1,
			],
			[
				'Report about urine',
				'Urine is okay, yellow color nice',
				1,
			],
			[
				'Report about body temperature',
				'Temperature is 36.6 deg: it is okay',
				1,
			],
			[
				'Report about blood',
				'Blood is not okay, patient will die',
				2,
			],

			[
				'Report about urine',
				'Urine is not okay, blue color is dead in future',
				2,
			],
			[
				'Report about body temperature',
				'Temperature is 15.1 deg: dead body',
				2,
			],
		];
		foreach ($content as $item) {
			$this->insert(Report::tableName(), [
				'testTitle' => $item[0],
				'result'    => $item[1],
				'patientId' => $item[2],
			]);
		}
	}

	public function safeDown() {

	}
}
