<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/**
 * @var       $this   yii\web\View
 * @var       $model  \app\models\Report
 * @var array $patients
 */
$this->title = 'Patient edit';
?>

<?php $form = ActiveForm::begin([
	'id'          => 'user-edit-form',
	'options'     => ['class' => 'form-vertical'],
	'fieldConfig' => [
		'template' => "<div class=\"row\"><div class=\"col-sm-4\">{label}</div>\n<div class=\"col-sm-8\">{input}{error}</div></div>",
	],
]); ?>

<?= $form->field($model, 'testTitle') ?>
<?= $form->field($model, 'result')->textarea() ?>
<?= $form->field($model, 'patientId')->dropDownList($patients) ?>

<div class="form-group">
	<?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
	<?= Html::a('Back to list', ['report-list'], ['class' => 'btn btn-link']) ?>
</div>

<?php ActiveForm::end(); ?>
