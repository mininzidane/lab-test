<?php

$config = [
	'id'         => 'skeleton',
	'language'   => 'en',
	'basePath'   => dirname(__DIR__),
	'bootstrap'  => ['log'],
	'components' => [
		'request'      => [
			// !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
			'cookieValidationKey' => 'cF-8JH7_9L7SZIDGEC0RbpEPUtg9PfJ0',
		],
		'cache'        => [
			'class' => 'yii\caching\FileCache',
		],
		'user'         => [
			'class'           => 'app\components\UserIdentity',
			'identityClass'   => 'app\models\Patient',
			'loginUrl'        => ['main/login'],
			'enableAutoLogin' => true,
		],
		'authManager' => [
			'class' => 'yii\rbac\PhpManager',
		],
		'errorHandler' => [
			'errorAction' => 'main/error',
		],
		'mailer'       => [
			'class'            => 'yii\swiftmailer\Mailer',
			// send all mails to a file by default. You have to set
			// 'useFileTransport' to false and configure a transport
			// for the mailer to send real emails.
			'useFileTransport' => true,
		],
		'log'          => [
			'traceLevel' => YII_DEBUG? 3: 0,
			'targets'    => [
				[
					'class'  => 'yii\log\FileTarget',
					'levels' => ['error', 'warning'],
				],
			],
		],
		'urlManager'   => [
			'cache'               => null,
			'suffix'              => '/',
			'enableStrictParsing' => true,
			'class'               => 'yii\web\UrlManager',
			'enablePrettyUrl'     => true,
			'showScriptName'      => false,
			'rules'               => require('_routes.php'),
		],
		'i18n' => [
			'translations' => [
				'*' => [
					'class'            => 'yii\i18n\PhpMessageSource',
					'basePath'         => "@app/messages",
					'forceTranslation' => true,
				],
			],
		],
		'view'         => [
			'class' => 'yii\web\View',
//			'renderers' => [
//				'tpl'  => [
//					'class'     => 'yii\twig\ViewRenderer',
//					'cachePath' => '@runtime/Twig/cache',
//				],
//				'twig' => [
//					'class'     => 'yii\twig\ViewRenderer',
//					// Array of twig options:
//					'options'   => [
//						'autoescape' => false,
//						'cache'      => YII_DEBUG? false: 'twig-cache',
//						'debug'      => YII_DEBUG,
//					],
//					'globals'   => [
//						'Html'       => '\yii\helpers\Html',
//						'GridView'   => '\yii\grid\GridView',
//						'Yii'        => 'Yii',
//						'Nav'        => '\app\widgets\Nav',
//						'AppAsset'   => '\app\assets\AppAsset',
//						'Url'        => '\yii\helpers\Url',
//						'ActiveForm' => '\yii\widgets\ActiveForm'
//					],
//					'functions' => [
//						'dump' => 'var_dump',
//						'die'  => 'die',
//					],
//					'uses'      => ['yii\bootstrap'],
//				],
//			]
		],
		'pdf' => [
			'class' => 'app\components\Pdf'
		],
	],
	'aliases'    => [
		'@views' => '@app/views',
	],
	'params'     => [

	],
];

if (YII_ENV_DEV) {
	// configuration adjustments for 'dev' environment
	$config['bootstrap'][]      = 'debug';
	$config['modules']['debug'] = 'yii\debug\Module';

	$config['bootstrap'][]    = 'gii';
	$config['modules']['gii'] = 'yii\gii\Module';
}

return $config;
