<?php
use \yii\bootstrap\Html;

/**
 * @var \app\models\Patient[] $models
 * @var yii\web\View          $this
 */
$this->title = 'Patient list';
$flash       = Yii::$app->session->getFlash('sendAuthKey');
?>

<?php if ($flash !== null) {
	echo Html::tag('div', $flash? 'Success': 'Error', ['class' => 'alert alert-' . ($flash? 'success': 'danger')]);
} ?>

<div class="form-group">
	<a href="<?= \yii\helpers\Url::to(['patient-add']) ?>" class="btn btn-lg btn-success">Add</a>
</div>

<?php if (count($models) == 0) { ?>
	<div class="alert alert-danger">No data</div>
	<?php return;
} ?>

<table class="table">
	<tr>
		<th><?= $models[0]->getAttributeLabel('id') ?></th>
		<th><?= $models[0]->getAttributeLabel('username') ?></th>
		<th><?= $models[0]->getAttributeLabel('email') ?></th>
		<th></th>
	</tr>

	<?php foreach ($models as $model) { ?>
		<tr>
			<td><?= $model->id ?></td>
			<td><?= $model->username ?></td>
			<td><?= $model->email ?></td>
			<td>
				<?= Html::a('<span class="glyphicon glyphicon-link"></span>', ['patient-send-auth-key', 'id' => $model->id], ['class' => 'btn btn-info']) ?>
				<?= Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['patient-edit', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
				<?= Html::a('<span class="glyphicon glyphicon-remove"></span>', ['delete', 'id' => $model->id], ['class' => 'btn btn-danger']) ?>
			</td>
		</tr>
	<?php } ?>
</table>
