<?php

namespace app\models;

/**
 * @property int     $id
 * @property string  $testTitle
 * @property string  $result
 * @property int     $patientId
 * @property Patient $patient
 *
 * Class Report
 * @package app\models
 */
class Report extends ActiveRecord {

	public function rules() {
		return [
			[['testTitle', 'result', 'patientId'], 'required'],
			[['patientId'], 'number'],
		];
	}

	public function getPatient() {
		return $this->hasOne(Patient::className(), ['id' => 'patientId']);
	}
}