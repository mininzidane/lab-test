Yii 2 Basic Application Template
================================

Yii 2 Basic Application Template is a skeleton Yii 2 application best for
rapidly creating small projects.

The template contains the basic features including user login/logout and a contact page.
It includes all commonly used configurations that would allow you to focus on adding new
features to your application.


DIRECTORY STRUCTURE
-------------------

      protected/assets/             contains assets definition
      protected/commands/           contains console commands (controllers)
      protected/config/             contains application configurations
      protected/controllers/        contains Web controller classes
      protected/mail/               contains view files for e-mails
      protected/models/             contains model classes
      protected/runtime/            contains files generated during runtime
      protected/tests/              contains various tests for the basic application
      protected/vendor/             contains dependent 3rd-party packages
      protected/views/              contains view files for the Web application
      public/                       contains the entry script and Web resources



REQUIREMENTS
------------

The minimum requirement by this application template that your Web server supports PHP 5.4.0.


INSTALLATION
------------

## Install via Composer

If you do not have [Composer](http://getcomposer.org/), you may install it by following the instructions
at [getcomposer.org](http://getcomposer.org/doc/00-intro.md#installation-nix).

All you need is to run next commands:
```bash
cd /path/to/project
# Install composer dependencies
composer install
```

## Multiple configuration files
Use APPLICATION_MODE = dev for development environment (it is in .htaccess file for Apache), it only needed for Yii2 internal debugger.
Use env.php to use your own configuration: @see protected/config/env.php (config below, file is not under version control).

## File structure
There are directories under .gitignore that must be created before application is started:
- public/assets
- protected/runtime

## Install frontend packages
Use [Bower](http://bower.io/) to install web packages:
```bash
cd /path/to/project
bower install
```

## DB config and data
Run migration tool. With migrations you can build all tables and insert initial test data to them
```bash
# Run migration
cd /path/to/project
./protected/yii migrate
```

## App config
For platform specified config use protected/config/env.php that contains something like:
```php
return [
	'components' => [
		'db' => [
			'class'    => 'yii\db\Connection',
			'dsn'      => 'mysql:host=localhost;dbname=[db_name]',
			'username' => '[user_name]',
			'password' => '[password]',
			'charset'  => 'utf8',
		],
	],
	'params'     => [

	],
];
```

## Unit tests
Create test DB, set DB config in /codeception.yml, fixtures are in /tests/_data/lab-test_2016-10-27.sql.
All unit test are run by command: 
```bash
composer exec codecept run unit
```

LAB ACCESS
----------
You can access to admin area using url /admin/ and credentials: admin/1234
For patient access you can use auth by hash /login-by-auth-key/?token=58108d5b58cbd4.15908389 (or one of the rest patients from DB).
Other way is to set password through admin panel for patient. And he will be able to login via login/password.

EMAILS
------
You can find sent emails in protected/runtime/mail. After sending email to client there will be an email with link for log in by auth key.
Also there will be emails with PDF from user export button 