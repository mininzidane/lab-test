<?php

namespace app\components;

class Pdf extends \yii\base\Component {

	/** @var \mPDF */
	private $_pdf;

	public function init() {
		parent::init();
		$this->_pdf = new \mPDF();
	}

	/**
	 * Set html that will be displayed in PDF
	 *
	 * @var string $html
	 */
	public function setHtml($html) {
		$this->_pdf->WriteHTML($html);
	}

	/**
	 * Get binary PDF output as string
	 *
	 * @return string
	 */
	public function getOutput() {
		return $this->_pdf->Output('', 'S');
	}
}