<?php

use app\models\LoginForm;
use app\models\Patient;

class LoginFormTest extends \Codeception\Test\Unit {

	/**
	 * @var \UnitTester
	 */
	protected $tester;

	protected function _before() {
	}

	protected function _after() {
	}

	// tests
	public function testLogin() {
		// login admin
		$loginForm           = new LoginForm(['scenario' => LoginForm::SCENARIO_ADMIN]);
		$loginForm->username = 'admin';
		$loginForm->password = '1234';
		$this->assertTrue($loginForm->login());

		// login patient by password
		$patient           = Patient::findOne(3);
		$patient->password = '1234';
		$patient->save(false);
		$loginForm           = new LoginForm();
		$loginForm->username = 'tom';
		$loginForm->password = '1234';
		$this->assertTrue($loginForm->login());

		// login patient by token
		$patient->password = null;
		$patient->save(false);
		$loginForm           = new LoginForm(['scenario' => LoginForm::SCENARIO_PATIENT_BY_TOKEN]);
		$loginForm->username = 'tom';
		$loginForm->token    = '78108d5b58cbd4.15908389';
		$this->assertTrue($loginForm->login());
	}
}