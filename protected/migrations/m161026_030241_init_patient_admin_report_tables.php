<?php

use app\models\Patient;
use app\models\Admin;
use app\models\Report;

class m161026_030241_init_patient_admin_report_tables extends \yii\db\Migration {

	public function up() {
		$this->createTable(Patient::tableName(), [
			'id'       => $this->primaryKey(),
			'username' => 'VARCHAR(128) NOT NULL',
			'password' => 'VARCHAR(32) NULL DEFAULT NULL',
			'email'    => 'VARCHAR(128) NULL DEFAULT NULL',
			'authKey'  => 'VARCHAR(128) NULL DEFAULT NULL',
			'created'  => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
		]);

		$this->createTable(Admin::tableName(), [
			'id'       => $this->primaryKey(),
			'username' => 'VARCHAR(128) NOT NULL',
			'password' => 'VARCHAR(32) NOT NULL',
			'email'    => 'VARCHAR(128) NULL DEFAULT NULL',
			'created'  => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
		]);

		$user = new \app\models\Admin();
		$this->insert(Admin::tableName(), [
			'username' => 'admin',
			'password' => $user->getHashedPassword('1234'),
		]);

		$this->createTable(Report::tableName(), [
			'id'        => $this->primaryKey(),
			'testTitle' => 'VARCHAR(128) NOT NULL',
			'result'    => 'TEXT NULL',
			'patientId' => 'INT(11) NOT NULL',
			'created'   => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
			'CONSTRAINT `report_patient` FOREIGN KEY (`patientId`) REFERENCES ' . Patient::tableName() . ' (`id`)',
		]);
	}

	public function down() {
		$this->dropTable(Report::tableName());
		$this->dropTable(Admin::tableName());
		$this->dropTable(Patient::tableName());
	}
}
