<?php

use yii\helpers\Html;

/**
 * @var       $this   yii\web\View
 * @var       $model  \app\models\Report
 */
?>
<h3><?= $model->getAttributeLabel('testTitle') ?></h3>
<div class="form-group"><?= $model->testTitle ?></div>

<h3><?= $model->getAttributeLabel('result') ?></h3>
<div class="form-group"><?= Html::decode($model->result) ?></div>