<?php

namespace app\models;

/**
 * @property int      $id
 * @property string   $username
 * @property string   $password
 * @property string   $email
 * @property string   $role
 * @property string   $authKey
 * @property Report[] $reports
 *
 * Class User
 * @package app\models
 */
class Patient extends ActiveRecord implements \yii\web\IdentityInterface {

	public $role = self::ROLE;
	public $passwordRepeat;

	const SALT         = 'me_cutie_super_safety_salt';
	const ROLE         = 'patient';
	const ACTIONS      = 'patientActions';
	const ID_SEPARATOR = '-';

	public function rules() {
		return [
			[['username', 'email'], 'required'],
			[['username'], 'unique'],
			[['role', 'passwordRepeat'], 'safe'],
			['password', 'compare', 'compareAttribute' => 'passwordRepeat', 'skipOnEmpty' => true],
		];
	}

	/**
	 * @inheritdoc
	 */
	public static function findIdentity($id) {
		$pieces = explode(self::ID_SEPARATOR, $id, 2);

		if ($pieces[0] == Admin::ROLE) {
			$model = Admin::findOne($pieces[1]);
		} else {
			$model = self::findOne($pieces[1]);
		}
		return $model;
	}

	/**
	 * @inheritdoc
	 */
	public static function findIdentityByAccessToken($token, $type = null) {
		return static::findOne(['authKey' => $token]);
	}

	/**
	 * Finds user by username
	 *
	 * @param  string $username
	 * @return static|null
	 */
	public static function findByUsername($username) {
		return self::find()->where('username = :username', [':username' => $username])->one();
	}

	/**
	 * @inheritdoc
	 */
	public function getId() {
		return static::ROLE . self::ID_SEPARATOR . $this->id;
	}

	/**
	 * @inheritdoc
	 */
	public function getAuthKey() {
		return $this->authKey;
	}

	/**
	 * @inheritdoc
	 */
	public function validateAuthKey($authKey) {
		return self::findIdentityByAccessToken($authKey) === null;
	}

	/**
	 * Validates password
	 *
	 * @param  string $password password to validate
	 * @return boolean if password provided is valid for current user
	 */
	public function validatePassword($password) {
		return $this->password === $this->getHashedPassword($password);
	}

	/**
	 * Return hashed password
	 *
	 * @param $password
	 * @return string
	 */
	public function getHashedPassword($password) {
		return md5(self::SALT . $password);
	}

	public function beforeSave($insert) {
		if (!parent::beforeSave($insert)) {
			return false;
		}

		if ($this->password) {
			$this->password = $this->getHashedPassword($this->password);
		}
		return true;
	}

	/**
	 * Get unique key for authorization
	 *
	 * @return string
	 */
	public function generateAuthKey() {
		while (true) {
			$authKey = uniqid('', true);
			if (!$this->validateAuthKey($authKey)) {
				continue;
			}

			$this->authKey = $authKey;
			$this->save(false, ['authKey']);
			return $authKey;
		}
		return false; // for IDE only, will never reach it
	}

	public function getReports() {
		return $this->hasMany(Report::className(), ['patientId' => 'id']);
	}
}