<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * Asset bundle for the Bower installed js/css files.
 */
class BowerAsset extends AssetBundle {

	public $sourcePath = '@bower';
	public $js         = [
	];
	public $css        = [
	];
}
