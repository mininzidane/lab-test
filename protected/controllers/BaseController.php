<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

class BaseController extends Controller {

	public $enableCsrfValidation = false;
	public $layout = 'main';

	/**
	 * Makes array data to view as JSON
	 *
	 * @param array $data
	 * @return string
	 */
	public function renderJson(array $data) {
		header('Content-Type: application/json; charset=utf-8');
		return json_encode($data, defined('JSON_UNESCAPED_UNICODE')? JSON_UNESCAPED_UNICODE: 0);
	}

	public function actions() {
		return [
			'error'   => [
				'class' => 'yii\web\ErrorAction',
				'view'  => '/main/error',
			],
			'captcha' => [
				'class'           => 'yii\captcha\CaptchaAction',
				'fixedVerifyCode' => YII_ENV_TEST? 'testme': null,
			],
		];
	}
}
