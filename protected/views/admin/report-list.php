<?php
use \yii\bootstrap\Html;

/**
 * @var \app\models\Report[] $models
 * @var yii\web\View         $this
 */
?>

<div class="form-group">
	<a href="<?= \yii\helpers\Url::to(['report-add']) ?>" class="btn btn-lg btn-success">Add</a>
</div>

<?php if (count($models) == 0) { ?>
	<div class="alert alert-danger">No data</div>
	<?php return;
} ?>

<table class="table">
	<tr>
		<th><?= $models[0]->getAttributeLabel('id') ?></th>
		<th><?= $models[0]->getAttributeLabel('testTitle') ?></th>
		<th><?= $models[0]->getAttributeLabel('patient') ?></th>
		<th></th>
	</tr>

	<?php foreach ($models as $model) { ?>
		<tr>
			<td><?= $model->id ?></td>
			<td><?= $model->testTitle ?></td>
			<td><?= $model->patient->username ?></td>
			<td>
				<?= Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['report-edit', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
				<?= Html::a('<span class="glyphicon glyphicon-remove"></span>', ['delete', 'id' => $model->id, 'model' => \app\models\Report::className()], ['class' => 'btn btn-danger']) ?>
			</td>
		</tr>
	<?php } ?>
</table>
