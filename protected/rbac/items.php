<?php

use \app\models\Patient;
use \app\models\Admin;

return [
	Admin::ACTIONS => [
		'type'        => 2,
		'description' => 'Admin actions',
	],
	Admin::ROLE    => [
		'type'     => 1,
		'children' => [
			Admin::ACTIONS,
		],
	],
	Patient::ACTIONS  => [
		'type'        => 2,
		'description' => 'User actions',
	],
	Patient::ROLE     => [
		'type'     => 1,
		'children' => [
			Patient::ACTIONS,
		]
	],
];
