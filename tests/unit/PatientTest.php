<?php

use \app\models\Patient;

class PatientTest extends \Codeception\Test\Unit {

	/**
	 * @var \UnitTester
	 */
	protected $tester;

	/**
	 * @var Patient
	 */
	protected $patient;

	protected function _before() {
		$this->patient = Patient::findOne(1);
	}

	protected function _after() {
	}

	// tests
	public function testGenerateAuthKey() {
		$authKey = $this->patient->generateAuthKey();
		$this->assertInternalType('string', $authKey);
		$this->tester->seeInDatabase('patient', ['authKey' => $authKey, 'id' => 1]);
	}

	public function testPasswordHashing() {
		$password          = '123456';
		$patient           = new Patient();
		$patient->username = 'test';
		$patient->email    = 'test@te.st';
		$patient->password = $password;
		$patient->save(false);
		$this->assertEquals($patient->getHashedPassword($password), $patient->password);
	}

	public function testFindIdentity() {
		$admin = Patient::findIdentity('admin-1');
		$this->assertEquals($admin->id, 1);
		$this->assertEquals($admin->role, 'admin');
		$patient = Patient::findIdentity('patient-2');
		$this->assertEquals($patient->id, 2);
		$this->assertEquals($patient->role, 'patient');
	}
}