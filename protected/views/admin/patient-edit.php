<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/**
 * @var $this   yii\web\View
 * @var $model  \app\models\Patient
 */
$this->title = 'Patient edit';
?>

<?php $form = ActiveForm::begin([
	'id'          => 'user-edit-form',
	'options'     => ['class' => 'form-vertical'],
	'fieldConfig' => [
		'template' => "<div class=\"row\"><div class=\"col-sm-4\">{label}</div>\n<div class=\"col-sm-8\">{input}{error}</div></div>",
	],
]); ?>

<?= $form->field($model, 'username') ?>
<?= $form->field($model, 'email') ?>
<?= $form->field($model, 'password')->passwordInput() ?>
<?= $form->field($model, 'passwordRepeat')->passwordInput() ?>

<div class="form-group">
	<?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
	<?= Html::a('Back to list', ['patient-list'], ['class' => 'btn btn-link']) ?>
</div>

<?php ActiveForm::end(); ?>
