# ************************************************************
# Sequel Pro SQL dump
# Version 4004
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.6.25)
# Database: lab-test
# Generation Time: 2016-10-27 05:40:48 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table admin
# ------------------------------------------------------------

DROP TABLE IF EXISTS `admin`;

CREATE TABLE `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(128) NOT NULL,
  `password` varchar(32) NOT NULL,
  `email` varchar(128) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;

INSERT INTO `admin` (`id`, `username`, `password`, `email`, `created`)
VALUES
	(1,'admin','34c11e9bdc09f9017676922c61b50c5f',NULL,'2016-10-26 15:38:46');

/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table patient
# ------------------------------------------------------------

DROP TABLE IF EXISTS `patient`;

CREATE TABLE `patient` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(128) NOT NULL,
  `password` varchar(32) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `authKey` varchar(128) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `patient` WRITE;
/*!40000 ALTER TABLE `patient` DISABLE KEYS */;

INSERT INTO `patient` (`id`, `username`, `password`, `email`, `authKey`, `created`)
VALUES
	(1,'michael',NULL,'michael@gmail.com','58108d5b58cbd4.15908389','2016-10-27 12:31:11'),
	(2,'jack',NULL,'jack@gmail.com','68108d5b58cbd4.15908389','2016-10-27 12:31:11'),
	(3,'tom',NULL,'tom@gmail.com','78108d5b58cbd4.15908389','2016-10-27 12:31:11'),
	(4,'john',NULL,'john@gmail.com','88108d5b58cbd4.15908389','2016-10-27 12:31:11'),
	(5,'andrew',NULL,'andrew@gmail.com','98108d5b58cbd4.15908389','2016-10-27 12:31:11');

/*!40000 ALTER TABLE `patient` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table report
# ------------------------------------------------------------

DROP TABLE IF EXISTS `report`;

CREATE TABLE `report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `testTitle` varchar(128) NOT NULL,
  `result` text,
  `patientId` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `report_patient` (`patientId`),
  CONSTRAINT `report_patient` FOREIGN KEY (`patientId`) REFERENCES `patient` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `report` WRITE;
/*!40000 ALTER TABLE `report` DISABLE KEYS */;

INSERT INTO `report` (`id`, `testTitle`, `result`, `patientId`, `created`)
VALUES
	(1,'Report about blood','Blood is okay, dark red very good',1,'2016-10-27 12:31:11'),
	(2,'Report about urine','Urine is okay, yellow color nice',1,'2016-10-27 12:31:11'),
	(3,'Report about body temperature','Temperature is 36.6 deg: it is okay',1,'2016-10-27 12:31:11'),
	(4,'Report about blood','Blood is not okay, patient will die',2,'2016-10-27 12:31:12'),
	(5,'Report about urine','Urine is not okay, blue color is dead in future',2,'2016-10-27 12:31:12'),
	(6,'Report about body temperature','Temperature is 15.1 deg: dead body',2,'2016-10-27 12:31:12');

/*!40000 ALTER TABLE `report` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
