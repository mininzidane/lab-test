<?php
use yii\helpers\Html;

/**
 * @var       $this   yii\web\View
 * @var       $model  \app\models\Report
 */
$this->title = 'Report detail #' . $model->id;
?>

<h3><?= $model->getAttributeLabel('testTitle') ?></h3>
<div class="form-group"><?= $model->testTitle ?></div>

<h3><?= $model->getAttributeLabel('result') ?></h3>
<div class="form-group"><?= Html::decode($model->result) ?></div>

<div class="form-group">
	<?= Html::a('<span class="glyphicon glyphicon-file"></span>', ['report-export', 'id' => $model->id], ['class' => 'btn btn-lg btn-success']) ?>
	<?= Html::a('<span class="glyphicon glyphicon-envelope"></span>', ['report-mail', 'id' => $model->id], ['class' => 'btn btn-lg btn-info']) ?>
	<?= Html::a('Back to list', ['index'], ['class' => 'btn btn-link']) ?>
</div>