<?php

use app\models\Patient;
use app\models\Report;

class ReportTest extends \Codeception\Test\Unit {

	/**
	 * @var \UnitTester
	 */
	protected $tester;

	/**
	 * @var Patient
	 */
	protected $patient;

	protected function _before() {
		$this->patient = Patient::findOne(3);
	}

	protected function _after() {
	}

	// tests
	public function testRelations() {
		$titles = [
			'test-relation',
			'test-relation-2'
		];
		$report            = new Report();
		$report->testTitle = $titles[0];
		$report->result    = 'test result relation';
		$report->patientId = 3;
		$report            = new Report();
		$report->testTitle = $titles[1];
		$report->result    = 'test result relation 2';
		$report->patientId = 3;
		foreach ($this->patient->reports as $i => $report) {
			$this->assertEquals($titles[$i], $report->testTitle);
		}
	}
}