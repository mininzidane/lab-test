<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\IdentityInterface;

/**
 * LoginForm is the model behind the login form.
 */
class LoginForm extends Model {

	const SCENARIO_PATIENT          = 'patient';
	const SCENARIO_PATIENT_BY_TOKEN = 'patientByToken';
	const SCENARIO_ADMIN            = 'admin';

	public $username;
	public $password;
	public $token;
	public $rememberMe = true;
	public $scenario = self::SCENARIO_PATIENT;

	protected $user;

	/**
	 * @return array the validation rules.
	 */
	public function rules() {
		$rules = [
			// username and password are both required
			[['username'], 'required'],
			// rememberMe must be a boolean value
			['rememberMe', 'boolean'],
			// password is validated by validatePassword()
			['password', 'validatePassword', 'skipOnEmpty' => true],
		];
		if ($this->scenario != self::SCENARIO_PATIENT_BY_TOKEN) {
			$rules[] = [['password'], 'required'];
		}
		return $rules;
	}

	/**
	 * Validates the password.
	 * This method serves as the inline validation for password.
	 *
	 * @param string $attribute the attribute currently being validated
	 * @param array  $params    the additional name-value pairs given in the rule
	 */
	public function validatePassword($attribute, $params) {
		if (!$attribute && $params['skipOnEmpty'] === true) {
			return;
		}
		if (!$this->hasErrors()) {
			$user = $this->getUser();

			if (!$user || !$user->validatePassword($this->password)) {
				$this->addError($attribute, 'Incorrect username or password.');
			}
		}
	}

	/**
	 * Logs in a user using the provided username and password.
	 * @return boolean whether the user is logged in successfully
	 */
	public function login() {
		if ($this->validate()) {
			$user = $this->getUser();

			if (!$user) {
				$this->addError('username', Yii::t('common', 'User not found'));
				return false;
			}

			$auth = Yii::$app->authManager;
			$role = $user->role?: Patient::ROLE; // by default -> patient

			if (!isset($auth->getAssignments($user->getId())[$role])) {
				$auth->assign($auth->getRole($role), $user->getId());
			}

			$duration = $this->rememberMe? 3600 * 24 * 30: 0;
			return Yii::$app->user->login($user, $duration);
		} else {
			return false;
		}
	}

	/**
	 * Finds user by [[username]]
	 *
	 * @return Patient|null
	 */
	public function getUser() {
		if (!$this->user) {
			switch ($this->scenario) {
				case self::SCENARIO_ADMIN:
					$this->user = Admin::findByUsername($this->username);
					break;
				default:
					$this->user = Patient::findByUsername($this->username);
			}
		}

		return $this->user;
	}
}
