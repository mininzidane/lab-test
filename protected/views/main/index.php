<?php
use yii\helpers\Html;

/**
 * @var                      $this yii\web\View
 * @var \app\models\Report[] $models
 */
$this->title = 'User reports';
$flash       = Yii::$app->session->getFlash('sendReportPdf');
?>

<?php if ($flash !== null) {
	echo Html::tag('div', $flash? 'Success': 'Error', ['class' => 'alert alert-' . ($flash? 'success': 'danger')]);
} ?>

<?php if (count($models) == 0) { ?>
	<div class="alert alert-danger">No data</div>
	<?php return;
} ?>

<table class="table">
	<tr>
		<th><?= $models[0]->getAttributeLabel('id') ?></th>
		<th><?= $models[0]->getAttributeLabel('testTitle') ?></th>
		<th></th>
	</tr>

	<?php foreach ($models as $model) { ?>
		<tr>
			<td><?= $model->id ?></td>
			<td><?= Html::a($model->testTitle, ['report-detail', 'id' => $model->id]) ?></td>
			<td>
				<?= Html::a('<span class="glyphicon glyphicon-file"></span>', ['report-export', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
				<?= Html::a('<span class="glyphicon glyphicon-envelope"></span>', ['report-mail', 'id' => $model->id], ['class' => 'btn btn-info']) ?>
			</td>
		</tr>
	<?php } ?>
</table>