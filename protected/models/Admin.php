<?php

namespace app\models;
use yii\base\Exception;

/**
 * Class Admin
 * @package app\models
 */
class Admin extends Patient {

	const ACTIONS = 'adminActions';
	const ROLE = 'admin';

	public $role = self::ROLE;

	public static function findIdentityByAccessToken($token, $type = null) {
		return null;
	}

	public function getAuthKey() {
		return null;
	}

	public function generateAuthKey() {
		throw new Exception('Method not allowed here');
	}
}