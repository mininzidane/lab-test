<?php
use yii\helpers\Html;

use yii\bootstrap\Nav;
//use yii\bootstrap\NavBar;
use app\assets\AppAsset;
use app\models\Product;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);

$action = Yii::$app->controller->action->id;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<meta charset="<?= Yii::$app->charset ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?= Html::csrfMetaTags() ?>
	<title><?= Html::encode($this->title) ?></title>
	<?php $this->head() ?>
</head>
<body style="padding-top: 15px">

<?php $this->beginBody() ?>
<?php if (!Yii::$app->user->isGuest) { ?>
	<div class="container">
		<div class="clearfix form-group">
			<div class="pull-right"><?= Yii::$app->user->getIdentity()->username ?>&nbsp;&nbsp;&nbsp;<a
					href="<?= Url::to(['main/logout']) ?>" class="btn btn-warning">Logout</a></div>
		</div>

		<?php
		$route = Yii::$app->controller->route;
		if (Yii::$app->user->getIdentity()->role == \app\models\Admin::ROLE) {
			$items = [
				[
					'label'  => 'Patients',
					'url'    => ['admin/patient-list'],
					'active' => in_array($route, ['admin/patient-list', 'admin/patient-add', 'admin/patient-edit']),
				],
				[
					'label'  => 'Reports',
					'url'    => ['admin/report-list'],
					'active' => in_array($route, ['admin/report-list', 'admin/report-add', 'admin/report-edit']),
				],
			];
		} else {
			$items = [
				[
					'label'  => 'User reports',
					'url'    => ['main/index'],
					'active' => in_array($route, ['main/index']),
				],
			];
		}
		echo Nav::widget([
			'options' => ['class' => 'nav-pills'],
			'items'   => $items,
		]);
		?>
	</div>
<?php } ?>
<div class="content">
	<div class="container">
		<h2 class="page-header"><?= $this->title ?></h2>
		<?= $content ?>
	</div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
