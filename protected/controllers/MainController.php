<?php

namespace app\controllers;

use app\models\Admin;
use app\models\Report;
use Yii;
use yii\base\Exception;
use yii\filters\AccessControl;
use app\models\LoginForm;
use app\models\Patient;
use yii\web\HttpException;

class MainController extends BaseController {

	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => ['login', 'login-by-auth-key', 'logout', 'error'],
						'allow'   => true,
					],
					[
						'actions' => ['index'],
						'allow'   => true,
						'roles'   => ['@'],
					],
					[
						'allow' => true,
						'roles' => [Patient::ROLE],
					],
				],
			],
		];
	}

	public function actionIndex() {
		if (Yii::$app->user->getIdentity()->role == Admin::ROLE) {
			return $this->redirect(['admin/index']);
		}

		/** @var Patient $patient */
		$patient = Yii::$app->user->getIdentity();
		return $this->render('index', [
			'models' => $patient->reports,
		]);
	}

	public function actionReportDetail($id) {
		$report = Report::findOne($id);
		if (!$report) {
			throw new HttpException(404, 'Report not found');
		}

		return $this->render('report-detail', [
			'model' => $report,
		]);
	}

	public function actionReportExport($id) {
		$report = Report::findOne($id);
		if (!$report) {
			throw new HttpException(404, 'Report not found');
		}

		$this->layout = false;
		/** @var \app\components\Pdf $pdf */
		$pdf = Yii::$app->pdf;
		$pdf->setHtml($this->render('report-as-pdf', [
			'model' => $report,
		]));

		$filename = date('Ymd') . "_report_{$id}.pdf";
		header('Content-type:application/pdf');
		header('Content-Disposition:attachment;filename=' . $filename);
		return $pdf->getOutput();
	}

	public function actionReportMail($id) {
		$report = Report::findOne($id);
		if (!$report) {
			throw new HttpException(404, 'Report not found');
		}

		$this->layout = false;
		/** @var \app\components\Pdf $pdf */
		$pdf = Yii::$app->pdf;
		$pdf->setHtml($this->render('report-as-pdf', [
			'model' => $report,
		]));
		$filename = date('Ymd') . "_report_{$id}.pdf";
		$sent     = Yii::$app->mailer
			->compose()
			->attachContent($pdf->getOutput(), ['fileName' => $filename, 'contentType' => 'application/pdf'])
			->setFrom('no-reply@lab.test')
			->setTo($report->patient->email)
			->setSubject("PDF attachment {$filename}")
			->send();
		Yii::$app->session->setFlash('sendReportPdf', $sent);
		$this->goHome();
	}

	public function actionLogin() {
		if (!\Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = new LoginForm();
		if ($model->load(Yii::$app->request->post()) && $model->login()) {
			return $this->goBack();
		} else {
			return $this->render('login', [
				'model' => $model,
			]);
		}
	}

	public function actionLoginByAuthKey($token) {
		Yii::$app->user->logout();

		$patient = Patient::findIdentityByAccessToken($token);
		if (!$patient) {
			throw new HttpException('Patient not found');
		}

		$model           = new LoginForm(['scenario' => LoginForm::SCENARIO_PATIENT_BY_TOKEN]);
		$model->username = $patient->username;
		$model->token    = $token;
		if (!$model->login()) {
			throw new Exception("Could not log in user: {$patient->username}");
		}
		$this->goBack();
	}

	public function actionLogout() {
		Yii::$app->user->logout();

		return $this->goHome();
	}
}
