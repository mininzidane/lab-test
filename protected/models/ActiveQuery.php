<?php

namespace app\models;

class ActiveQuery extends \yii\db\ActiveQuery {

	public function getTableName() {
		$modelName = $this->modelClass;
		return $modelName::tableName();
	}

	public function resetScopes() {
		$this->where = '';
		return $this;
	}
}