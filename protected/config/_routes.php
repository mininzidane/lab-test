<?php

return [
	'/'                => 'main/index',
	'/admin/'          => 'admin/index',
	'/admin/<action>/' => 'admin/<action>',
	'/<action>/'       => 'main/<action>',
];
