<?php

namespace app\controllers;

use app\models\ActiveRecord;
use app\models\Admin;
use app\models\Report;
use \Yii;
use yii\bootstrap\Html;
use yii\filters\AccessControl;
use app\models\Patient;
use app\models\LoginForm;
use yii\helpers\ArrayHelper;
use yii\web\HttpException;

class AdminController extends BaseController {

	public function behaviors() {
		return [
			'access' => [
				'class'        => AccessControl::className(),
				'rules'        => [
					[
						'actions' => ['login', 'logout', 'error'],
						'allow'   => true,
					],
					[
						'allow' => true,
						'roles' => [Admin::ROLE],
					],
				],
				'denyCallback' => function ($rule, $action) {
					/** @var Patient $user */
					$user = Yii::$app->user->getIdentity();
					if ($user && $user->role != Admin::ROLE) {
						throw new HttpException(403, 'You are not allowed to perform this action');
					}
					/** @var \yii\base\InlineAction $action */
					return $this->redirect(['login']);
				},
			],
		];
	}

	public function actionIndex() {
		return $this->redirect(['patient-list']);
	}

	public function actionPatientList() {
		$models = Patient::find()->all();
		return $this->render('patient-list', [
			'models' => $models,
		]);
	}

	public function actionPatientAdd() {
		$model = new Patient();

		if (Yii::$app->request->isPost) {
			if ($model->load(Yii::$app->request->post()) && $model->save()) {
				return $this->redirect(['patient-list']);
			}
		}

		return $this->render('patient-edit', [
			'model' => $model,
		]);
	}

	public function actionPatientEdit($id) {
		$model = Patient::findOne($id);

		if (Yii::$app->request->isPost) {
			if ($model->load(Yii::$app->request->post()) && $model->save()) {
				return $this->redirect(['patient-list']);
			}
		}

		return $this->render('patient-edit', [
			'model' => $model,
		]);
	}

	/**
	 * Send link with auth key to user email
	 *
	 * @param int $id
	 */
	public function actionPatientSendAuthKey($id) {
		$model   = Patient::findOne($id);
		$authKey = $model->generateAuthKey();

		$body = Html::a('Link to log in', ['main/login-by-auth-key', 'token' => $authKey]);
		$sent = Yii::$app->mailer
			->compose()
			->setTextBody($body)
			->setFrom('no-reply@lab.test')
			->setTo($model->email)
			->setSubject('Link for log in')
			->send();
		Yii::$app->session->setFlash('sendAuthKey', $sent);
		$this->goBack();
	}

	public function actionReportList() {
		$models = Report::find()->all();
		return $this->render('report-list', [
			'models' => $models,
		]);
	}

	public function actionReportAdd() {
		$model = new Report();

		if (Yii::$app->request->isPost) {
			if ($model->load(Yii::$app->request->post()) && $model->save()) {
				return $this->redirect(['report-list']);
			}
		}

		return $this->render('report-edit', [
			'model'    => $model,
			'patients' => ArrayHelper::map(Patient::find()->all(), 'id', 'username'),
		]);
	}

	public function actionReportEdit($id) {
		$model = Report::findOne($id);

		if (Yii::$app->request->isPost) {
			if ($model->load(Yii::$app->request->post()) && $model->save()) {
				return $this->redirect(['report-list']);
			}
		}

		return $this->render('report-edit', [
			'model'    => $model,
			'patients' => ArrayHelper::map(Patient::find()->all(), 'id', 'username'),
		]);
	}

	/**
	 * Common delete action
	 *
	 * @param int         $id
	 * @param null|string $model Fully qualified class name for model to delete. 'Patient' by default
	 * @throws HttpException
	 */
	public function actionDelete($id, $model = null) {
		if (!$model) {
			$model = Patient::className();
		}

		/** @var ActiveRecord $model */
		$model = $model::findOne($id);
		if (!$model) {
			throw new HttpException(404, "Model #{$id} not found");
		}

		$model->delete();
		$this->goBack();
	}

	public function actionLogin() {
		if (!\Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = new LoginForm(['scenario' => LoginForm::SCENARIO_ADMIN]);
		if ($model->load(Yii::$app->request->post()) && $model->login()) {
			return $this->goBack();
		} else {
			return $this->render('/main/login', [
				'model' => $model,
			]);
		}
	}
}