<?php

namespace app\components;

use app\models\Admin;
use app\models\Patient;

class UserIdentity extends \yii\web\User {

	private $_cleanId;

	public $role;

	public function init() {
		parent::init();
		$fullId = $this->getId();
		if ($fullId === null) {
			return;
		}
		$pieces         = explode(Patient::ID_SEPARATOR, $fullId, 2);
		$this->_cleanId = $pieces[1];
		if ($pieces[0] == Admin::ROLE) {
			$this->identityClass = 'app\models\Admin';
			$this->loginUrl      = ['admin/login'];
		}
		$this->role = $this->getIdentity()->role;
	}

	public function getCleanId() {
		return $this->_cleanId;
	}
}
 