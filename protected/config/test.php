<?php

$config = require 'console.php';

return array_replace_recursive($config, [
	'components' => [
		'db'          => [
			'class'    => 'yii\db\Connection',
			'dsn'      => 'mysql:host=localhost;dbname=lab-test-test',
			'username' => 'root',
			'password' => '28051989',
			'charset'  => 'utf8',
		],
		'authManager' => [
			'class' => 'yii\rbac\PhpManager',
		],
		'user'        => [
			'class'           => 'app\components\UserIdentity',
			'identityClass'   => 'app\models\Patient',
			'loginUrl'        => ['main/login'],
			'enableAutoLogin' => true,
		],
		'request'     => [
			// !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
			'cookieValidationKey' => 'cF-8JH7_9L7SZIDGEC0RbpEPUtg9PfJ0',
		],
	],
	'params'     => [

	],
]);